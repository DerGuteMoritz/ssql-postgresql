(module ssql-postgresql

(*postgresql-translator*)

(import chicken scheme)
(use ssql postgresql foops data-structures)

(define *postgresql-translator* 
  (let ((type->sql-converters 
         `((,boolean? . boolean->sql)
           ,@(*ansi-translator* 'type->sql-converters)))
        (clauses-order (append (*ansi-translator* 'clauses-order)
                               '(returning))))

    (derive-object (*ansi-translator* self)
                   ((escape-string string)
                    (escape-string (ssql-connection) string))

                   ((boolean->sql boolean)
                    (if boolean "'t'" "'f'"))

                   ((clauses-order) clauses-order)

                   ((type->sql-converters) type->sql-converters)

                   ((array (elements ...))
                    (sprintf "ARRAY[~A]"
                             (string-intersperse 
                              (map (lambda (el)
                                     (self 'ssql->sql el))
                                   elements)
                              ", "))))))

(define-operators *postgresql-translator*
  (limit prefix)
  (offset prefix)
  (returning prefix "RETURNING" ", ")
  (random function)
  (@> infix)
  (<@ infix))

(register-sql-engine! connection? *postgresql-translator*)

)